**Kacha Transaction Backend**



1. Description
Kacha Transaction Backend is a simple Node.js backend designed to handle HTTP POST and GET requests. It is connected with a MongoDB database to store and retrieve transaction data.


2. Features
Handles HTTP POST requests to create new transactions
Supports HTTP GET requests to retrieve transaction data
Utilizes MongoDB for data storage

3. Requirements
Node.js
MongoDB

4. Installation
Clone the repository: git clone https://gitlab.com/i3129/kacha_transaction_backend.git
Navigate to the project directory: cd Kacha_Transaction_Backend
Install dependencies: npm install

5. Configuration
Ensure MongoDB is running on your system or update the MongoDB connection settings in config.js to match your environment.
Make any additional configuration changes as necessary in config.js.

6. Usage
Start the server: npm start
Send HTTP POST requests to create transactions.
Send HTTP GET requests to retrieve transaction data.

7. Endpoints
POST /api/transactions: Create a new transaction
GET /transactions: Retrieve all transactions


Example
curl -X POST -H "Content-Type: application/json" -d '{"amount": 50, "catagory": "Grocery shopping"}' http://localhost:5000/api/transactions

8. The code is in the master branch
